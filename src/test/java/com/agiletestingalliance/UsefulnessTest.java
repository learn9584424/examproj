package com.agiletestingalliance;

import org.junit.Assert;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class UsefulnessTest {

    @Test
    public void testDesc() {
        Usefulness usefulness = new Usefulness();
        String result = usefulness.desc();
        String expected = "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.";
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testFunctionWF() {
        Usefulness usefulness = new Usefulness();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        usefulness.functionWF();
        String expectedOutput = "0\n1\n2\n3\n4\n";
        Assert.assertEquals(expectedOutput, outContent.toString());
    }
}