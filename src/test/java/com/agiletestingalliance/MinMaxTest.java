package com.agiletestingalliance;

import org.junit.Assert;
import org.junit.Test;

public class MinMaxTest {

    @Test
    public void testF() {
        MinMax minMax = new MinMax();
        int result = minMax.foo(5, 10);
        Assert.assertEquals(10, result);
    }

    @Test
    public void testBar() {
        MinMax minMax = new MinMax();
        String result = minMax.bar("Hello");
        Assert.assertEquals("Hello", result);
    }

    @Test
    public void testBarWithNull() {
        MinMax minMax = new MinMax();
        String result = minMax.bar(null);
        Assert.assertNull(result);
    }

    @Test
    public void testBarWithEmptyString() {
        MinMax minMax = new MinMax();
        String result = minMax.bar("");
        Assert.assertEquals("", result);
    }
}